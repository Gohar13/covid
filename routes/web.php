<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@login');

    $router->group(['middleware' => 'auth'], function () use ($router) {

        $router->get('verifyPhone',  ['uses' => 'UserController@verifyPhone']);
        $router->post('verifyPhone',  ['uses' => 'UserController@verifyPhone']);

        $router->group(['middleware' => 'verifiedUser'], function () use ($router) {
            $router->get('logout', 'AuthController@logout');
            $router->get('user',  ['uses' => 'UserController@showOne']);
            $router->post('addPatient',  ['uses' => 'UserController@addPatient']);

            $router->get('testKit/uploadVideo/{id}', ['uses' => 'TestKitController@uploadVideoView']);
            $router->post('testKit/addVideo/{id}', ['uses' => 'TestKitController@addVideo']);

            $router->group(['middleware' => 'testedKit'], function () use ($router) {
                $router->get('testKit/uploadResult/{id}', ['uses' => 'TestKitController@uploadVideoView']);
                $router->post('testKit/addResult/{id}', ['uses' => 'TestKitController@addTestResult']);
            });
        });

        $router->group(['prefix' => 'agent', 'middleware' => 'agent'], function () use ($router) {
                $router->get('users', 'UserController@showAllUsers');
                $router->get('users/{id}', 'UserController@showUser');
                $router->get('users/testKit{id}', 'UserController@showTestKit');
        });
    });
});



