<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestKitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_kits', function (Blueprint $table) {
            $table->id();
            $table->string('serial_number');
            $table->string('video')->nullable();
            $table->string('image')->nullable();
            $table->foreignId('status_id')->nullable()->constrained('test_kit_statuses');
            $table->foreignId('user_id')->constrained('users');
            $table->boolean('tested')->nullable();
            $table->enum('display_type', ['1', '2', '3', '4'])->nullable();
            $table->timestamp('tested_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_kits');
    }
}
