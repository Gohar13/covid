<?php

namespace Database\Seeders;

use App\Models\TestKitStatus;
use Illuminate\Database\Seeder;

class KitStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = TestKitStatus::STATUSES;

        foreach ($statuses as $status) {
            TestKitStatus::query()->create(['status' => $status]);
        }
    }
}
