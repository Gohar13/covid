<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TestKitStatus;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KitStatusSeeder::class);
    }
}
