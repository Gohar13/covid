<?php

namespace App\Http\Controllers;


use App\Models\TestKit;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TestKitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /** Upload video page */

    public function uploadVideoView($id)
    {
        $testKit = TestKit::query()->find($id);

        if ($testKit) {
            return $this->responseRequestSuccess($testKit);
        }

        return 'error';

    }
    /** Update  */
    public function addVideo(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $this->validate($request, [
            'video' => 'required|file',
        ]);

        if ($request->hasFile('video')) {
            $file = saveFile($request->file('video'), 'testKits/video');

            if ($file) {
                try {
                    $testKit = TestKit::query()->find($id);

                    if ($testKit) {
                        $testKit->video = $file;
                        $testKit->tested = 1;
                        $testKit->save();
                    }

                    //return successful response
                    return $this->responseRequestSuccess(['testKit' => $testKit]);

                } catch (\Exception $e) {
                    //return error message
                    return $this->responseRequestError($e->getMessage(), 409);
                }

            } else {
                return $this->responseRequestError('Cannot upload file');
            }
        } else {
            return $this->responseRequestError('File not found', 409);
        }
    }

    public function uploadTestResultView($id): \Illuminate\Http\JsonResponse
    {
        $testKit = TestKit::query()->find($id);

        if ($testKit) {
            return $this->responseRequestSuccess($testKit);
        }

        return 'error';
    }

    public function addTestResult(Request $request, $id)
    {


    }
}
