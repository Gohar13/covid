<?php

namespace App\Http\Controllers;

use App\Models\TestKit;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function showOne(): JsonResponse
    {
        $user = Auth::user();
        $patientsIds = $user->patients()->pluck('id')->toArray();
        array_push($patientsIds, $user->id);
        $testKits = TestKit::query()->with('user')->whereIn('id', $patientsIds)->get();
        $data = [
            'userData' => $user,
            'testKits' => $testKits,
        ];

        return $this->responseRequestSuccess($data);
    }

    public function verifyPhone(Request $request): JsonResponse
    {
        $user = Auth::user();
        if ($request->method() == 'GET') {
            $code = generateIntegerCode(4);
            $user->verification_code = $code;
            $user->save();

            return $this->responseRequestSuccess(['user' => $user]);
        } else {
            $this->validate($request, [
                'code' => 'required',
            ]);
            $code = $request->input('code');
            if ($user->verified || $code == $user->verification_code) {
                $user->verification_code = null;
                $user->verified = 1;
                $user->save();

                return $this->responseRequestSuccess(['user' => $user]);
            } else {
                return $this->responseRequestError('Invalid code');
            }
        }
    }

    /**
     * Store a new patient.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function addPatient(Request $request): JsonResponse
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'id_password' => 'required|unique:users',
            'phone' => 'required|unique:users',
            'password_image' => 'required|file',
            'serial_number' => 'required|string',
        ]);

        if ($request->hasFile('password_image')) {
            $file = saveFile($request->file('password_image'));

            if ($file) {
                try {
                    $user = new User;
                    $user->first_name = $request->input('first_name');
                    $user->last_name = $request->input('last_name');
                    $user->id_password = $request->input('id_password');
                    $user->phone = $request->input('phone');
                    $user->password_image = $file;
                    $user->owner_id = Auth::id();
                    $user->save();

                    $testKit = new TestKit();
                    $testKit->user_id = $user->id;
                    $testKit->serial_number = $request->input('serial_number');
                    $testKit->save();

                    //return successful response
                    return $this->responseRequestSuccess(['user' => $user]);

                } catch (\Exception $e) {
                    //return error message
                    return $this->responseRequestError($e->getMessage(), 409);
                }
            } else {
                return $this->responseRequestError('Cannot upload file');
            }
        } else {
            return $this->responseRequestError('File not found', 409);
        }
    }
}
