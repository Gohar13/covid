<?php

namespace App\Http\Controllers;

use App\Models\TestKit;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * Store a new user.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'id_password' => 'required|unique:users',
            'phone' => 'required|unique:users',
            'password_image' => 'required|file',
            'serial_number' => 'required|string',
        ]);

        if ($request->hasFile('password_image')) {
            $file = saveFile($request->file('password_image'));

            if ($file) {
                try {
                    $user = new User;
                    $user->first_name = $request->input('first_name');
                    $user->last_name = $request->input('last_name');
                    $user->id_password = $request->input('id_password');
                    $user->phone = $request->input('phone');
                    $user->password_image = $file;
                    $user->save();

                    $testKit = new TestKit();
                    $testKit->user_id = $user->id;
                    $testKit->serial_number = $request->input('serial_number');
                    $testKit->save();

                    //return successful response
                    return $this->responseRequestSuccess(['user' => $user]);

                } catch (\Exception $e) {
                    //return error message
                    return $this->responseRequestError($e->getMessage(), 409);
                }

            } else {
                return $this->responseRequestError('Cannot upload file');
            }
        } else {
            return $this->responseRequestError('File not found', 409);
        }
    }

    /**
     * Get a JWT via given credentials.
     *
     * @param Request $request
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $this->validate($request, [
            'phone' => 'required|string',
            'id_password' => 'required|string',
        ]);

        $credentials = $request->only(['phone', 'id_password']);
        $user = User::where($credentials)->first();

        if ($user) {
            /*   if (!$user->verified) {
                   return $this->responseRequestError('User Not verified');
               } elseif ($token = JWTAuth::fromUser($user)) {
                   return $this->respondWithToken($token);
               }*/
            if ($token = JWTAuth::fromUser($user)) {
                return $this->respondWithToken($token);
            }
        } else {
            return $this->responseRequestError('User Not Found', 401);
        }

        if ($user && $token = JWTAuth::fromUser($user)) {
            return $this->respondWithToken($token);
        } else {
            return response()->json(['message' => 'Unauthorized'], 401);
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        return response()->json([
            'message' => 'User logged off successfully!'
        ], 200);
    }

}
