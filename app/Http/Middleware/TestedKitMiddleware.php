<?php

namespace App\Http\Middleware;

use App\Models\TestKit;
use Closure;
use Illuminate\Support\Facades\Auth;

class TestedKitMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $testKitId = $request->route('id');

        if ($testKitId && $testKit = TestKit::query()->find($testKitId)) {

            if ($testKit->tested) {
                return $next($request);
            }

            return response()->json(['status' => 'error', 'message' => 'Video is not uploaded', 'code' => 409], 409);
        }
        return response()->json(['status' => 'error', 'message' => 'Test kit not found', 'code' => 401], 401);
    }
}
