<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TestKitStatus extends Model
{
    public $timestamps = false;

    const STATUS_NEGATIVE = 'negative';
    const STATUS_POSITIVE = 'positive';
    const STATUS_DEFECTED = 'defected';

    const STATUSES = [
        self::STATUS_POSITIVE,
        self::STATUS_NEGATIVE,
        self::STATUS_DEFECTED
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function testKits(): HasMany
    {
        return $this->hasMany(TestKit::class, 'status_id');
    }

}



