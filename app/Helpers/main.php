<?php

function generateIntegerCode($length = 25): string
{
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function saveFile($file, $folderToUpload = 'users/password-images'): ?string
{
    $fileName = $file->getClientOriginalName();
    $original_filename_arr = explode('.', $fileName);
    $file_ext = end($original_filename_arr);
    $destination_path = "./upload/$folderToUpload";
    $image = 'U-' . time() . '.' . $file_ext;
    if ($file->move($destination_path, $image)) {
        return $image;
    } else {
        return null;
    }
}
